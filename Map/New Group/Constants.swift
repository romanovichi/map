//
//  Constants.swift
//  Map
//
//  Created by Иван Романович on 17.08.2018.
//  Copyright © 2018 Иван Романович. All rights reserved.
//

import Foundation

struct Urls {
    static let artwork = "https://data.honolulu.gov/resource/csir-pcj2.json"
}

struct Constants {
    static let regionRadius: Double = 1000
    static let numberOfRowsForArtworkObjectDetails = 3
}

struct Cells {
    static let firstCell = 0
    static let secondCell = 1
    static let thirdCell = 2
}
