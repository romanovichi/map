//
//  DataService.swift
//  Map
//
//  Created by Иван Романович on 17.08.2018.
//  Copyright © 2018 Иван Романович. All rights reserved.
//

import Foundation

class DataService {
    
    let apiClient = ApiClient()
    
    func getArtworkData(_ completion: @escaping (ArtworkList)->()) {
        apiClient.getArtworkData { (json) in
            let artwork = ArtworkList.from(json: json)
            completion(artwork)
        }
    }
    
}
