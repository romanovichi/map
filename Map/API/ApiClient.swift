//
//  ApiClient.swift
//  Map
//
//  Created by Иван Романович on 17.08.2018.
//  Copyright © 2018 Иван Романович. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class ApiClient {
    
    func getArtworkData(completion: @escaping (JSON)->()) {
        Alamofire.request(Urls.artwork, method: .get).responseJSON { (response) in
            if response.result.error == nil {
                guard let data = response.data else { return }
                if let json = try? JSON(data: data) {
                    completion(json)
                } else {
                    print("Error in ApiClient \(String(describing: response.result.error))")
                }
            }
        }
    }
    
}



