//
//  ImageCell.swift
//  Map
//
//  Created by Иван Романович on 19.08.2018.
//  Copyright © 2018 Иван Романович. All rights reserved.
//

import UIKit
import Kingfisher

class ImageCell: UITableViewCell {

    @IBOutlet weak var artworkImageView: UIImageView!
    
    func setImage(_ imageStringURL: String) {
        let url = URL(string: imageStringURL)
        let image = UIImage(named: "placeholder")
        artworkImageView.kf.setImage(with: url, placeholder: image)
        
    }
    
}
