//
//  DetailsTableViewAdapter.swift
//  Map
//
//  Created by Иван Романович on 18.08.2018.
//  Copyright © 2018 Иван Романович. All rights reserved.
//

import Foundation
import UIKit

class DetailsTableViewAdapter: NSObject {
    
    private var table: UITableView!
    private var artwork: ArtworkObject?
    
    init(tableView: UITableView) {
        
        table = tableView
        table.registerCell(.weatherCell)
        table.registerCell(.imageCell)
        
        super.init()
        
        table.rowHeight = UITableViewAutomaticDimension
        table.estimatedRowHeight = 60
        table.allowsSelection = false
        table.tableFooterView = UIView()
        
        table.delegate = self
        table.dataSource = self
    }
    
    func receive(_ artwork: ArtworkObject) {
        self.artwork = artwork
        table.reloadData()
    }
}

extension DetailsTableViewAdapter: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Constants.numberOfRowsForArtworkObjectDetails
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == Cells.firstCell {
            if let cell = tableView.dequeueReusableCell(of: .weatherCell, for: indexPath) as? TextCell {
                if let artwork = artwork {
                    cell.setDescriptionLabel(with: artwork.discipline)
                    return cell
                }
            }
        }
        if indexPath.row == Cells.secondCell {
            if let cell = tableView.dequeueReusableCell(of: .weatherCell, for: indexPath) as? TextCell {
                if let artwork = artwork {
                    cell.setDescriptionLabel(with: artwork.descriptionText)
                    return cell
                }
            }
        }
        if indexPath.row == Cells.thirdCell {
            if let cell = tableView.dequeueReusableCell(of: .imageCell, for: indexPath) as? ImageCell {
                if let artwork = artwork {
                    cell.setImage(artwork.imageFile)
                    return cell
                }
            }
        }
        return UITableViewCell()
    }
}
