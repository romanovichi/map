//
//  TableViewCell.swift
//  Map
//
//  Created by Иван Романович on 18.08.2018.
//  Copyright © 2018 Иван Романович. All rights reserved.
//

import UIKit

class TextCell: UITableViewCell {
    
    @IBOutlet weak var label: UILabel!
    
    func setDescriptionLabel(with text: String) {
        label.text = text
    }
}
