//
//  MapVC.swift
//  Map
//
//  Created by Иван Романович on 08.08.2018.
//  Copyright © 2018 Иван Романович. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class MapVC: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var currentLocationButtonWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var pullUpView: UIView!
    @IBOutlet weak var pullUpViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var pullUpViewTitleLabel: UILabel!
    
    private var mapAdapter: MapViewAdapter!
    private var tableAdapter: DetailsTableViewAdapter!
    
    let dataService = DataService()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        mapAdapter = MapViewAdapter(mapView: mapView)
        mapAdapter.delegate = self
        
        tableAdapter = DetailsTableViewAdapter(tableView: tableView)
        
        addSwipe()
        
        dataService.getArtworkData { (artwork) in
            self.mapAdapter.updateArtworkList(with: artwork)
        }
    }
}

extension MapVC {

    @IBAction func centerMapButtonWasPressed(_ sender: Any) {
        mapAdapter.centerMapOnUserLocation()
    }
}

extension MapVC: MapViewAdapterDelegate {
    
    func pass(_ artwork: ArtworkObject) {
        tableAdapter.receive(artwork)
        pullUpViewTitleLabel.text = artwork.title!
        showDetails()
    }
    
    func showDetails() {
        pullUpViewHeightConstraint.constant = 300
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func animateViewUp() {
        switch pullUpViewHeightConstraint.constant {
        case 300:
            pullUpViewHeightConstraint.constant = 600
            currentLocationButtonWidthConstraint.constant = 0
        default:
            pullUpViewHeightConstraint.constant = 300
        }
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func animateViewDown() {
        switch pullUpViewHeightConstraint.constant {
        case 600:
            pullUpViewHeightConstraint.constant = 300
            currentLocationButtonWidthConstraint.constant = 50
        default:
            pullUpViewHeightConstraint.constant = 1
        }
        
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    func addSwipe() {
        
        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(animateViewUp))
        swipeUp.direction = .up
        pullUpView.addGestureRecognizer(swipeUp)
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(animateViewDown))
        swipeDown.direction = .down
        pullUpView.addGestureRecognizer(swipeDown)
    }
}
