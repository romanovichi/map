//
//  MapService.swift
//  Map
//
//  Created by Иван Романович on 09.08.2018.
//  Copyright © 2018 Иван Романович. All rights reserved.
//

import Foundation
import CoreLocation
import MapKit

protocol MapViewAdapterDelegate: class {
    func pass(_ artwork: ArtworkObject)
}

class MapViewAdapter: NSObject {
    
    private let map: MKMapView!
    
    var locationManager = CLLocationManager()
    let authorizationStatus = CLLocationManager.authorizationStatus()
    weak var delegate: MapViewAdapterDelegate?
    
    init(mapView: MKMapView) {
        
        map = mapView
        mapView.showsUserLocation = true
    
        super.init()
        
        configureLocationServices()
        
        map.register(ArtworkMarkerView.self,
                         forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)
        
        let initialLocation = CLLocationCoordinate2D(latitude: 21.282778, longitude: -157.829444)
        center(on: initialLocation)
        
        map.delegate = self
    }
}

extension MapViewAdapter {
    
    func center(on coordinate: CLLocationCoordinate2D) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(coordinate, Constants.regionRadius * 2.0, Constants.regionRadius * 2.0)
        map.setRegion(coordinateRegion, animated: true)
    }
    
    func updateArtworkList(with artworkList: ArtworkList) {
        map.addAnnotations(artworkList.list )
    }
}

extension MapViewAdapter: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if let annotation = view.annotation {
            let artwork = annotation as! ArtworkObject
            center(on: annotation.coordinate)
            delegate?.pass(artwork)
        }
    }
    
    func centerMapOnUserLocation() {
        if authorizationStatus == .authorizedAlways || authorizationStatus == .authorizedWhenInUse {
            guard let coordinate = locationManager.location?.coordinate else { return }
            center(on: coordinate)
        }
    }
}

extension MapViewAdapter: CLLocationManagerDelegate {
    
    func configureLocationServices() {
        if authorizationStatus == .notDetermined {
            locationManager.requestAlwaysAuthorization()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        centerMapOnUserLocation()
    }
}
