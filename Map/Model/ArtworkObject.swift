//
//  DroppablePin.swift
//  Map
//
//  Created by Иван Романович on 11.08.2018.
//  Copyright © 2018 Иван Романович. All rights reserved.
//

import Foundation
import MapKit

class ArtworkObject: NSObject, MKAnnotation {
    
    let title: String?
    let locationName: String
    let discipline: String
    let descriptionText: String
    let imageFile: String
    let latitude: Double
    let longitude: Double
    
    var coordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
    
    var subtitle: String? {
        return locationName
    }
    
    var markerTintColor: UIColor  {
        switch discipline {
        case "Monument":
            return #colorLiteral(red: 0.5568627715, green: 0.3529411852, blue: 0.9686274529, alpha: 1)
        case "Mural":
            return #colorLiteral(red: 0.3647058904, green: 0.06666667014, blue: 0.9686274529, alpha: 1)
        case "Plaque":
            return #colorLiteral(red: 0.1215686277, green: 0.01176470611, blue: 0.4235294163, alpha: 1)
        case "Sculpture":
            return #colorLiteral(red: 0.1764705926, green: 0.01176470611, blue: 0.5607843399, alpha: 1)
        default:
            return #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
        }
    }

    init(_ title: String, _ locationName: String, _ discipline: String, _ description: String, _ imageFile: String, _ latitude: Double, _ longitude: Double) {
        self.title = title
        self.locationName = locationName
        self.discipline = discipline
        self.descriptionText = description
        self.imageFile = imageFile
        self.latitude = latitude
        self.longitude = longitude
        
        super.init()
    }
}
