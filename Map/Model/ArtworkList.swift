//
//  ArtworkList.swift
//  Map
//
//  Created by Иван Романович on 17.08.2018.
//  Copyright © 2018 Иван Романович. All rights reserved.
//

import Foundation
import SwiftyJSON

class ArtworkList {
    
    var list = [ArtworkObject]()
    
    static func from(json: JSON) -> ArtworkList {
        
        let artworkData = json.arrayValue
        let artworkList = ArtworkList()
        
        artworkData.forEach { (artwork) in
            
            let title = artwork["title"].stringValue
            let locationName = artwork["location"].stringValue
            let discipline = artwork["discipline"].stringValue
            let description = artwork["description"].stringValue
            let imageFile = artwork["imagefile"].stringValue
            let latitude = artwork["latitude"].doubleValue
            let longitude = artwork["longitude"].doubleValue
            
            artworkList.list.append(ArtworkObject(title, locationName, discipline, description, imageFile, latitude, longitude))
        }
        return artworkList
    }
    
}
